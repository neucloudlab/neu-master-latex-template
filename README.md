# 东北大学硕士论文Latex模板

## （1）介绍

自用东北大学硕士论文latex模板。请谨慎使用，若存在不符合论文要求的，请注意修改或提Request。若有因滥用而无法满足毕业需求的，本人概不负责。

## （2）安装教程

推荐的使用方法是：

1. 使用[Overleaf](https://www.overleaf.com)，并设置使用xelatex作为编译引擎。
2. 使用[Docker](https://hub.docker.com)，在env文件夹下提供dockerfile。
3. 自助安装latex编译器如texlive，mactex，ctex等，编辑器推荐VS Code。

dockerfile 在编译过程中会自动配置texlive、ssh、和本模板所需的中文字体。注意：需下载大约4.5GB的原镜像。

```shell
cd <path-of-env>
docker build --name cloudlab-texlive . 
docker run -itd --network bridge --name cloudlab-texlive -p 2222:22 -v <本地路径>:/home/cloudlab/latex cloudlab-texlive 
```

在vs code中使用remote ssh 通过ssh进入docker。账号密码均为cloudlab

```shell
ssh cloudlab@localhost -p 2222
```

## （3）使用说明


### 1. 文件组织

```shell
.
├── FigureData           tikz绘图文件夹
│   ├── Figure-Chapter1  tikz绘图文件夹-章节1 
│   ├── Figure-Chapter2
│   ├── Figure-Chapter3
│   ├── Figure-Chapter4
│   ├── Figure-Chapter5
│   └── Figure-Chapter6
├── Figures              普通图片文件
└── env                  dockerfile文件夹
```

### 2. 参数设定

根据自身情况在main.tex中修改如下参数

```latex
\classification{} % 分类号
\serialno{} % 秘级
\udc{} % UDC
\zhtitle{东北大学硕士论文模板(中文-示例)} % 中文标题
\entitle{Master Thesis Template of Northeastern University(zh)} % 英文标题
\zhauthor{作者} % 作者姓名（中文）
\enauthor{Author} % 作者姓名（英文）
% 单导师使用以下配置
%\zhsupervisoritem{\makebox[3.5cm][s]{指导教师:} }
%\zhsupervisor{李四\quad 教授 \qquad 东北大学软件学院}

% 双导师使用以下配置
\zhsupervisoritem{\makebox[3.5cm][s]{指导教师:} \\ ~}
\zhsupervisor{李四\quad 教授 \qquad  东北大学软件学院 \\ 李四\quad 教授 \qquad  东北大学软件学院}
\zhdegreetype{硕士} % 学位级别
\zhmajortype{工学} % 学位类型
\zhmajor{软件工程} % 专业名称
\zhsubmissiondate{20xx年6月} % 论文提交时间
\zhdefensedate{20xx年6月} % 答辩时间
\zhdegreegivendate{20xx年6月} % 学位授予时间
\zhdefensechairmen{主席} % 答辩主席
\zhreviewers{评阅人1\quad 评阅人2 } % 评阅人姓名
\zhthesisdate{20xx年6月} % 论文首页日期（中文）
\enthesisdate{June 20xx} % 论文首页日期（英文）
\ensupervisor{Supervisor: Associate Professor Li Si} % 指导教师（英文）
```

### 3. 编译技巧

本模板的文件结构如下。各个部分通过input命令链接至main.tex中。在实际使用中，可以仅保留正在撰写的部分，注释掉其他部分。

```shell
main.tex
├── declaration.tex
├── zhabstract.te
├── enabstract.texx
├── chapter1.texx
├── chapter1.tex
├── chapter2.tex
├── chapter3.tex
├── chapter4.tex
├── chapter5.tex
├── chapter6.tex
├── thanks.tex
└── situation.tex
```

在本模板main.tex默认关闭了编译压缩选项，有利于加速编译。编译最终版时请注释本选项，降低最终pdf的文件大小。

```latex
\documentclass{neumaster}
% 关闭文件压缩，加速编译。编译最终版时请注释本选项
\special{dvipdfmx:config z 0}
```

### 4. tikz绘图技巧

由于tikz撰写过程中需要大量尝试过程。所以本模板提供figure-pdf.tex文件。figure-pdf.tex，main.tex 文件均引用tikzbase.cls。tikzbase.cls中配置默认的tikz配置。目前仅配置了推荐的色卡、统一的字体字号。

若需撰写FigureData/Figure-Chapter2/figure-optimization-process.tex 文件，建议在figure-pdf.tex中通过input的方式调试。完成后在引入至主文件中。

## （4）参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request